const fs = require('fs');
const request = require('supertest');
const app = require('../app/app');

jest.mock('fs');
jest.mock('request-promise', () => url =>
	new Promise((resolve, reject) => {
		if (url === 'https://www.example.com/bad-feed') reject();

		const html = `<!DOCTYPE html>
		<html>
		<head>
			<title>Feed</title>
		</head>
		<body>
			<article>
				<h2><a href="https://www.example.com/article_1">Title A</a></h2>
				<div class="author">Author</div>
				<img class="image" src="https://www.example.com/image.jpeg">
				<div class="date">January 17, 2019</div>
				<p>Summary</p>
			</article>
			<article>
				<h2><a href="https://www.example.com/article_2">Title B</a></h2>
				<div class="author">Author</div>
				<div class="image" style="background-image:url('https://www.example.com/image.jpeg')">
				<div class="date">January 17, 2019</div>
				<p>Summary</p>
			</article>
		</body>
		</html>`;

		resolve(html);
	})
);

describe('App', () => {
	test('should return 404 for a non-existent source', done => {
		request(app)
			.get('/feed')
			.then(res => {
				expect(res.statusCode).toBe(404);
				done();
			});
	});

	test('should return 400 for a misconfigured source', done => {
		fs.existsSync = jest.fn();
		fs.existsSync.mockReturnValue(true);

		fs.readFileSync = jest.fn();
		fs.readFileSync.mockReturnValue('{}');

		request(app)
			.get('/feed')
			.then(res => {
				expect(res.statusCode).toBe(400);
				done();
			});
	});

	test('should return 400 for a error when updating source', done => {
		const config = `{
			title: 'Feed',
			url: 'https://www.example.com/bad-feed',
			selectors: {
				article: 'article',
				title: 'h2',
				author: '.author',
				image: '.image',
				date: '.date',
				link: 'h2 a',
				summary: 'p'
			},
			transformer: article => ({})
		}`;

		fs.existsSync = jest.fn();
		fs.existsSync.mockReturnValue(true);

		fs.readFileSync = jest.fn();
		fs.readFileSync.mockReturnValue(config);

		request(app)
			.get('/feed')
			.then(res => {
				expect(res.statusCode).toBe(400);
				done();
			});
	});

	test('should return 200 for a correctly configured source', done => {
		const config = `{
			title: 'Feed',
			url: 'https://example.com/feed',
			selectors: {
				article: 'article',
				title: 'h2',
				author: '.author',
				image: '.image',
				date: '.date',
				link: 'h2 a',
				summary: 'p'
			},
			transformer: article => {
				if (article.title === 'Title B') return false;

				return {
					title: article.title.toUpperCase()
				}
			}
		}`;

		fs.existsSync = jest.fn();
		fs.existsSync.mockReturnValue(true);

		fs.readFileSync = jest.fn();
		fs.readFileSync.mockReturnValue(config);

		request(app)
			.get('/feed')
			.then(res => {
				expect(res.statusCode).toBe(200);
				done();
			});
	});

	test('should return 200 for a correctly configured source with missing attributes', done => {
		const config = `{
			title: 'Feed',
			url: 'https://example.com/feed',
			selectors: {
				article: 'article',
				title: 'h2',
				date: '.date',
				link: 'h2 a'
			}
		}`;

		fs.existsSync = jest.fn();
		fs.existsSync.mockReturnValue(true);

		fs.readFileSync = jest.fn();
		fs.readFileSync.mockReturnValue(config);

		request(app)
			.get('/feed')
			.then(res => {
				expect(res.statusCode).toBe(200);
				done();
			});
	});

	test('should return 200 for a correctly configured source with non-resolving attributes', done => {
		const config = `{
			title: 'Feed',
			url: 'https://example.com/feed',
			selectors: {
				article: 'article',
				title: 'h2',
				date: '.missing-date',
				link: 'h2 a',
				author: '.missing-author',
				summary: '.missing-summary',
				image: '.missing-image',
			}
		}`;

		fs.existsSync = jest.fn();
		fs.existsSync.mockReturnValue(true);

		fs.readFileSync = jest.fn();
		fs.readFileSync.mockReturnValue(config);

		request(app)
			.get('/feed')
			.then(res => {
				expect(res.statusCode).toBe(200);
				done();
			});
	});
});
