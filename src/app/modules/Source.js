const RSS = require('rss');
const request = require('request-promise').defaults({jar: true});
const {
	JSDOM
} = require('jsdom');

const Joi = require('joi');
const SourceSchema = require('./SourceSchema');

class Source {
	constructor(url, config) {
		const v = Joi.validate(config, SourceSchema);

		if (v.error) throw v.error;

		this.config = config;
		this.parse = this.parse.bind(this);
		this.get = this.get.bind(this);
		this.addFeedItems = this.addFeedItems.bind(this);
		this.feed = new RSS({
			title: config.title,
			feed_url: url,
			site_url: config.url
		});
	}

	parse(html) {
		return new Promise(resolve => {
			const {
				selectors,
				transformer
			} = this.config;

			const dom = new JSDOM(html);
			const articles = [];

			const articlesDom = [].slice.call(
				dom.window.document.querySelectorAll(selectors.article)
			);

			articlesDom.forEach(articleDom => {
				let article = {};

				['title', 'author', 'date'].forEach(key => {
					const ele = articleDom.querySelector(selectors[key]);
					if (ele) article[key] = ele.textContent;
				});

				article.summary = '';
				if (selectors.summary) {
					const ele = articleDom.querySelector(selectors.summary);

					if (ele) article.summary = ele.innerHTML;
				}

				if (selectors.image) {
					const imageElement = articleDom.querySelector(selectors.image);

					if (imageElement) {
						switch (imageElement.tagName) {
							case 'IMG':
								article.image = imageElement.getAttribute('src');
								if (!article.image) article.image = imageElement.getAttribute('data-src');

								break;
							default:
								article.image = imageElement.style.backgroundImage
									.slice(4, -1)
									.replace(/['"]/g, '');
								break;
						}
					}
				}

				const ele = articleDom.querySelector(selectors.link);
				article.link = ele.getAttribute('href');

				if (article.date) article.date = new Date(article.date);

				if (transformer) {
					const transformed = transformer({ ...article
					});

					Object.keys(article).forEach(key => {
						const value = transformed[key];

						if (value && article[key].constructor === value.constructor)
							article[key] = value;
					});

					if (transformed === false) article = null;
				}

				if (article && article.title && article.date && article.link)
					articles.push(article);
			});

			resolve(articles);
		});
	}

	addFeedItems(items) {
		items.forEach(item => {
			this.feed.item({
				title: item.title,
				description: `<img src="${item.image}" alt="">${item.summary}`,
				url: item.link,
				author: item.author,
				date: item.date
			});
		});
	}

	get() {
		return new Promise((resolve, reject) => {
			let cookies = this.config.cookies || (() => new Promise(resolve => resolve()));

			cookies(request).then(body => {
				request({
					uri: this.config.url,
					simple: false
				})
					.then(this.parse)
					.then(this.addFeedItems)
					.then(() => resolve(this.feed.xml({
						indent: true
					})))
					.catch(reject);
			});
		});
	}
}

module.exports = Source;
