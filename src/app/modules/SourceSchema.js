const Joi = require('joi');

module.exports = Joi.object()
	.keys({
		title: Joi.string().required(),
		url: Joi.string()
			.uri()
			.required(),
		selectors: Joi.object()
			.keys({
				article: Joi.string().required(),
				title: Joi.string().required(),
				author: Joi.string(),
				image: Joi.string(),
				date: Joi.string().required(),
				link: Joi.string().required(),
				summary: Joi.string()
			})
			.required(),
		transformer: Joi.func().arity(1),
		cookies: Joi.func().arity(1)
	})
	.required();
