const fs = require('fs');
const express = require('express');

const app = express();
const Source = require('./modules/Source');

app.get('/:source', (req, res) => {
	const filename = `${__dirname}/../sources/${req.params.source}.js`;
	const exists = fs.existsSync(filename);

	if (!exists) return res.sendStatus(404);

	try {
		const rawFile = fs.readFileSync(filename);
		const config = new Function(`return ${rawFile}`);
		const source = new Source(
			`http://feed.brambogaerts.nl/${req.params.source}`,
			config()
		);

		source
			.get()
			.then(rss => res.send(rss))
			.catch(e => res.status(400).send(e));
	} catch (e) {
		res.status(400).send(e);
	}
});

module.exports = app;
