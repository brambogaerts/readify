[![pipeline status](https://gitlab.com/brambogaerts/readify/badges/master/pipeline.svg)](https://gitlab.com/brambogaerts/readify/commits/master)
[![coverage report](https://gitlab.com/brambogaerts/readify/badges/master/coverage.svg)](https://gitlab.com/brambogaerts/readify/commits/master)

# 🤓 Readify

Readify allows you to generate RSS feeds from any webpage using CSS selectors.

## 🤷‍ Why?

Because not every webpage offers RSS feeds for their content, making it difficult to unify their posts in a single place to read.

## 🚀 Quickstart

Create a source in the `/sources/` directory. A source is a JavaScript object that contains information about how to scrape the target webpage.

For example:

```javascript
// /sources/pocket-must-reads.js
{
	title: 'Pocket Must Reads',								// required
	url: 'https://getpocket.com/explore/must-reads',		// required
	selectors: {
		article: 'article.item_article',					// required
		title: '.item_content h3.title a.link_track',		// required
		author: '.item_content cite.domain a',
		image: 'a.item_link.link_track div.item_image.image_active',
		date: '.item_content cite.domain span.read_time',	// required
		link: '.item_content h3.title a.link_track',		// required
		summary: '.item_content p.excerpt'
	},
	transformer: article => ({
		link: new URL(article.link).searchParams.get('url'),
		image: new URL(article.image).searchParams.get('url')
	})
};
```

## 💅 Transformer

The transformer is a function that can be optionally added to a source. It is called for every article found on the target webpage. It takes an article as an argument, and must return an object containing transformed values.

For example:

```javascript
//...
transformer: article => ({
	title: article.title.toUpperCase()
})
//...
```
